package actor

import (
	"sync"
	"fmt"
	"log"
	"time"
)
// Actor
type Actor struct {
	actionChan chan action
	lastOutput string
	wg         *sync.WaitGroup
}
type action func()

func NewActor(wg *sync.WaitGroup) *Actor {
	fmt.Println("NewActor()")
	a := &Actor{}
	a.actionChan = make(chan action, 254)
	a.lastOutput = ""
	a.wg = wg
	return a
}

// Create NewActor and Run it
func StartActor(wg *sync.WaitGroup) *Actor {
	a := NewActor(wg)
	a.Run()
	return a
}

// Run Actor -> Stop will be implemented later
func (a *Actor) Run() {
	fmt.Println("Actor.Run()")
	go a.runLoop()
}

func (a *Actor) runLoop() {
	fmt.Println("Actor.runLoop()")
	timer := time.NewTimer(2 * time.Minute)
	for {
		select {
		case action := <-a.actionChan:
			action()
			a.wg.Done()
		case <-timer.C:
			log.Println("Time out!")
		}

	}
}

func (a *Actor) AddIP(ip string) {
	fmt.Println("Actor.AddIP:", ip)
	a.wg.Add(1)
	a.actionChan <- func() {
		fmt.Println("PING:", ip)
		cmd := exec.Command("ping", ip, "-n", "1", "-w", "1")
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Run()
		a.lastOutput = out.String()
	}
}

func (a *Actor) String() string {
	fmt.Println("Actor.String()")
	resultChan := make(chan string, 0)
	a.wg.Add(1)
	a.actionChan <- func() {
		resultChan <- a.status()
	}
	return <-resultChan
}

func (a *Actor) status() string {
	fmt.Println("Actor.status()")
	r := a.lastOutput
	a.lastOutput = ""
	return r
}
